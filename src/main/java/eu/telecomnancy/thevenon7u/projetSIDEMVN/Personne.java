package eu.telecomnancy.thevenon7u.projetSIDEMVN;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class Personne {
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotNull
    private Date birthDate;
    private String citizenship;
    @NotNull
    @Min(value=18)
    private Integer age;

    public Personne(@NotBlank String firstName, @NotBlank String lastName, @NotNull Date birthDate, String citizenship) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
	this.citizenship = citizenship;

        this.age = 20;
    }
}
