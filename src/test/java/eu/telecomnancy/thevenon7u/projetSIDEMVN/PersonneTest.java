package eu.telecomnancy.thevenon7u.projetSIDEMVN;

import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Date;
import java.util.Set;

import static org.junit.Assert.assertEquals;


public class PersonneTest {

    @Test
    public void test() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Personne p = new Personne("Victor", "Thevenon", new Date(01, 01, 2001), "French");
        Set<ConstraintViolation<Personne>> constraintViolations = validator.validate(p);
        assertEquals(0, constraintViolations.size());
    }
}
